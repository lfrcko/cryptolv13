﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {
    public class CryptoCurrency {
        public CryptoCurrency(string name, string shortName, decimal stanje) {
            Name = name;
            ShortName = shortName;
            Stanje = stanje;
        }

        override public string ToString() {
            return Name;
        }

        public string Ispis { get {
                return Stanje + " " + ShortName;
            }
        }

        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal Stanje { get; set; }
    }


}
